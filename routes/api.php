<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

// Route::middleware('auth:api'){
// 	//Route::post('vendor/logout', 'VendorController@logout');
//
// }

Route::middleware('auth:vendorapi')->group(function () {
		//Route::post('customer/login', 'Auth\LoginController@login');
		Route::get('category', 'CategoryController@categories');
});

Route::post('vendor/login', 'VendorController@login');

Route::post('vendor/register', 'VendorController@register');

Route::post('vendor/logout', 'VendorController@logout');

Route::post('customer/login', 'Auth\LoginController@login');

Route::post('customer/register', 'Auth\RegisterController@register');

//Route::get('category', 'CategoryController@categories');

Route::post('search', 'SearchController@search');

Route::get('category/{category}', 'CategoryController@show');

Route::resource('products', 'ProductController');

Route::post("new-store",'StoreController@create');

Route::get('location', 'LocationController');



Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
