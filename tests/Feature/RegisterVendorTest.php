<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterVendorTest extends TestCase
{
  public function testsRegisterIsSuccessful()
  {
      $data = [
          'email' => 'email@customer.com',
          'password' => 'password',
          'password_confirmation' => 'password',
          'firstname' => 'Johnny',
          'lastname' => 'Blaze',
          'phone' => '00000006566',
          'gender' => 'm'
      ];
      $headers = ['X-api-key' => env('APP_KEY')];

      $this->json('post', '/api/vendor/register', $data, $headers)
      ->assertStatus(201)
      ->assertJsonStructure([
          'success' => ['token','user_id']
      ]);
  }
}
