<?php

namespace App\Listeners;

use App\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification as listener;

class SendEmailVerificationNotification extends listener implements ShouldQueue
{
    use InteractsWithQueue;
}
