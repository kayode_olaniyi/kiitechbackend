<?php

namespace App\Http\Middleware;

use Closure;

class VerifyApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(null == $request->headers->has('X-api-key') && $request->header('X-api-key') !== env('APP_KEY'))
        {
           return response()->json(['error' => 'Forbidden'], 403);
        }
        return $next($request);
    }
}
