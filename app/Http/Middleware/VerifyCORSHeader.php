<?php

namespace App\Http\Middleware;

use Closure;

class VerifyCORSHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      return $next($request)
        //->header('Access-Control-Allow-Origin', $_SERVER['HTTP_ORIGIN'])
        //->header('Access-Control-Allow-Origin', '*')
        // ->header('Access-Control-Allow-Origin', 'http://localhost')
        ->header('Access-Control-Allow-Methods', 'GET, DELETE, POST, PUT, OPTIONS')
        ->header('Access-Control-Allow-Credentials', 'true')
        ->header('Access-Control-Allow-Headers', 'Accept, Content-Type, Authorization, X-Requested-With, X-api-key');
  }
}
