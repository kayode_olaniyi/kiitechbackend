<?php

namespace App\Http\Controllers;

use App\Repository\StoreRepository;
use Illuminate\Http\Request;
use Validator;

class StoreController extends Controller
{
    private $storeRepository;

    public function __construct(StoreRepository $storeRepository)
    {
        $this->storeRepository = $storeRepository;
    }

    //
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255|unique:stores',
            'address' => 'required|string|min:13',
            'state' => 'required',
            'city' => 'required',
            'category_id' => 'required',
            'category_child_id' => 'required',
            'production_quantity_id' => 'required',
            'vendor_type' => 'required',
            'vendor_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $response = $this->storeRepository->create($request->all());
        return response()->json(['response' => $response]);
    }
}
