<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductAvatar;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return response()->json(['data' => $products], 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $this->validate($request, [
        'avatars' => 'required',
        'avatars.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'name' => 'required|string|max:255',
        'price' => 'required|numeric',
      ]);

        $product = new Product;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->vendor_id = Auth::guard('vendorapi')->id;
        $product->brief_description = $request->brief_description;
        $product->detail_description = $request->detail_description;
        $product->save();

        // if($request->hasfile('avatars'))
        //  {
        //
        //    foreach ($request->avatars as $avatar) {
        //      $filename = $photo->store('avatars');
        //      ProductsAvatar::create([
        //          'product_id' => $product->id,
        //          'name' => $filename
        //      ]);
        //   }
        //  }
        return response()->json(['data' => "product with $product->id created"], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return response()->json(['data' => $products], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
          'avatars' => 'required',
          'avatars.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          'name' => 'required|string|max:255',
          'price' => 'required|numeric',
        ]);

        $product->name = $request->name;
        $product->price = $request->price;
        $product->vendor_id = Auth::guard('vendorapi')->id;
        $product->brief_description = $request->brief_description;
        $product->detail_description = $request->detail_description;
        $product->save();

        // if($request->hasfile('avatars'))
        //  {
        //
        //    foreach ($request->avatars as $avatar) {
        //      $filename = $photo->store('avatars');
        //      ProductsAvatar::create([
        //          'product_id' => $product->id,
        //          'name' => $filename
        //      ]);
        //   }
        //  }
        return response()->json(['data' => "product with $product->id created"], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        Product::destroy($product);
    }
}
