<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{

  public function categories($parent_id = 0, $child_id = '')
  {
    $results = DB::table('categories')->where('parent_id', $parent_id)->get();
    $categories = array();
    if(($results->count()) > 0)
    {
      foreach($results as $result)
      {
        $category = array();
        $category['id'] = $result->id;
        $category['name'] = $result->name;
        $category['parent_id'] = $result->parent_id;
        $category['subcategories'] = category($result->id);
        $categories[$result->id] = $category;
      }
    }
    return $categories;
  }
    //return category(); //response()->json(['data' => category()], 204);
}
