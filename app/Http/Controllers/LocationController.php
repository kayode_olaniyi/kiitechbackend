<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    public function __invoke()
    {
      $state = DB::table('states')->get();
      $city = DB::table('cities')->get();
      return response()->json(['states' => $state, 'cities' => $city]);
    }
}
