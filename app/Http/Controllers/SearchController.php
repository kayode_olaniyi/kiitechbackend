<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

	public function search(Request $request)
  {
    /*
    * @search from Product
    */
		$product  =  DB::table('products')->where('name', 'LIKE', '%' .$request->search .'%')->orWhere('description', 'LIKE', '%'.$request->search.'%')->paginate(5);

    //$searchItem = array(); //store all search data for pagination

    return response()->json(['data' => $product], 201);
  }
}
