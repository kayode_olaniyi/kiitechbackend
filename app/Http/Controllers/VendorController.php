<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Vendor;

class VendorController extends Controller
{

    public function login(Request $request)
    {
      $validator = Validator::make($request->all(), [
         'email' => 'required|email|exists:vendors',
         'password' => 'required|string|min:8',
      ]);

      if ($validator->fails()){
          return response()->json(['error'=>$validator->errors()], 401);
      }


      if(Vendor::where('email', $request->email)->exists())
      {
         $user = Vendor::where('email', $request->email)->first();
         $auth = Hash::check($request->password, $user->password);
         if($user && $auth)
         {
            $user->generateToken(); //Model Method
            return response([
               'currentUser' => $user,
               'message' => 'Login Successful!',
            ]);
        }
      }
    }

    public function register(Request $request)
    {
       $validator = Validator::make($request->all(), [
         'firstname' => 'required|string|max:255',
         'lastname' => 'required|string|max:255',
         'phone' => 'required|string|min:11|unique:users',
         'email' => 'required|string|email|max:255|unique:users',
         'password' => 'required|string|min:6|confirmed',
         'gender' => 'required'
       ]);


       if ($validator->fails()) {
           return response()->json(['error'=>$validator->errors()], 401);
       }

       $input = $request->all();
       $input['password'] = Hash::make($input['password']);
       $vendor = Vendor::create($input);
       $vendor->generateToken();
       $success['token'] =  $vendor->api_token;
       $success['user_id'] =  $vendor->id;
       return response()->json(['success'=>$success], 201);
   }

   public function logout(Request $request)
   {
        $user = Auth::guard('vendorapi')->user();

        if ($user) {
            $user->api_token = null;
            $user->save();
        }

        return response()->json(['data' => 'User logged out.'], 200);
  }

  public function guard()
  {
    return Auth::guard('vendorapi');
  }
}
