<?php

namespace App\Providers;

use App\Repository\StoreRepository;
use App\Store;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StoreRepository::class, function ($app) {
            return new StoreRepository(new Store());
        });
    }
}
