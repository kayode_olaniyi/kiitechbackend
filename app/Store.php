<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = [
        'name', 'slug', 'address', 'category_id', 'category_child_id',
        'production_quantity_id', 'vendor_type', 'vendor_id'
    ];
    protected $table = 'stores';
}
