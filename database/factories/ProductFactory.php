<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => mt_rand(),
        'owner' => $faker->company,
        'description' => $faker->text,
    ];
});
