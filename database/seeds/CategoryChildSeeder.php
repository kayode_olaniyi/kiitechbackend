<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryChildSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('category_child')->insert([
        //fashion here
        ['category_id' => 1, 'child_id' => 11 ],
        ['category_id' => 1, 'child_id' => 12 ],
        ['category_id' => 1, 'child_id' => 13 ],
        ['category_id' => 1, 'child_id' => 14 ],
        ['category_id' => 1, 'child_id' => 15 ],
        ['category_id' => 1, 'child_id' => 16 ],
        ['category_id' => 1, 'child_id' => 17 ],
        ['category_id' => 1, 'child_id' => 18 ],
        ['category_id' => 1, 'child_id' => 19 ],
        ['category_id' => 1, 'child_id' => 20 ],
        ['category_id' => 1, 'child_id' => 21 ],
        ['category_id' => 1, 'child_id' => 22 ],
        ['category_id' => 1, 'child_id' => 23 ],
        ['category_id' => 1, 'child_id' => 24 ],
        ['category_id' => 1, 'child_id' => 25 ],
        ['category_id' => 1, 'child_id' => 26 ],
        ['category_id' => 1, 'child_id' => 27 ],
        ['category_id' => 1, 'child_id' => 28 ],
        ['category_id' => 1, 'child_id' => 29 ],
        //furniture
        ['category_id' => 2, 'child_id' => 30 ],
        ['category_id' => 2, 'child_id' => 31 ],
        ['category_id' => 2, 'child_id' => 32 ],
        ['category_id' => 2, 'child_id' => 33 ],
        ['category_id' => 2, 'child_id' => 34 ],
        ['category_id' => 2, 'child_id' => 35 ],
        ['category_id' => 2, 'child_id' => 36 ],
        ['category_id' => 2, 'child_id' => 37 ],
        ['category_id' => 2, 'child_id' => 38 ],
        ['category_id' => 2, 'child_id' => 39 ],
        ['category_id' => 2, 'child_id' => 40 ],
        ['category_id' => 2, 'child_id' => 41 ],
        //Food
        ['category_id' => 3, 'child_id' => 42 ],
        ['category_id' => 3, 'child_id' => 43 ],
        ['category_id' => 3, 'child_id' => 44 ],
        ['category_id' => 3, 'child_id' => 45 ],
        ['category_id' => 3, 'child_id' => 46 ],
        ['category_id' => 3, 'child_id' => 47 ],
        ['category_id' => 3, 'child_id' => 48 ],
        ['category_id' => 3, 'child_id' => 49 ],
        ['category_id' => 3, 'child_id' => 50 ],

        //graphics and Branding
        ['category_id' => 6, 'child_id' => 51 ],
        ['category_id' => 6, 'child_id' => 52 ],
        ['category_id' => 6, 'child_id' => 53 ],
        ['category_id' => 6, 'child_id' => 54 ],
        ['category_id' => 6, 'child_id' => 55 ],
        ['category_id' => 6, 'child_id' => 56 ],
        ['category_id' => 6, 'child_id' => 57 ],
        ['category_id' => 6, 'child_id' => 58 ],
        ['category_id' => 6, 'child_id' => 59 ],
        ['category_id' => 6, 'child_id' => 60 ],
        ['category_id' => 6, 'child_id' => 61 ],
        ['category_id' => 6, 'child_id' => 62 ],
        ['category_id' => 6, 'child_id' => 63 ],

      ]);
  }
}
