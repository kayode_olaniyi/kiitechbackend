<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MyCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->delete();

      DB::table('categories')->insert([
        ['id' => 1, 'parent_id' => 0, 'name' => 'Fashion', 'slug' => 'fashion'],
        ['id' => 2, 'parent_id' => 0, 'name' => 'Furniture', 'slug' => 'furniture'],
        ['id' => 3, 'parent_id' => 0, 'name' => 'Food', 'slug' => 'food'],
        ['id' => 4, 'parent_id' => 0, 'name' => 'Architecture', 'slug' => 'architecture'],
        ['id' => 5, 'parent_id' => 0, 'name' => 'Packaging', 'slug' => 'packaging'],
        ['id' => 6, 'parent_id' => 0, 'name' => 'Branding', 'slug' => 'branding'],
        ['id' => 7, 'parent_id' => 0, 'name' => 'Interior Decoration', 'slug' => 'interior-decoration'],
        ['id' => 8, 'parent_id' => 0, 'name' => 'Engineering', 'slug' => 'engineering'],
        ['id' => 9, 'parent_id' => 0, 'name' => 'Commerce', 'slug' => 'commerce'],
        ['id' => 10, 'parent_id' => 0, 'name' => 'Graphics', 'slug' => 'graphics'],

        /*
         * Fashion Category starts here
         * main sub-category: Men, Women, Kids
        */

        ['id' => 11,  'parent_id' => 1, 'name' => 'Men', 'slug' => 'men'],
        ['id' => 12,  'parent_id' => 1, 'name' => 'Women', 'slug' => 'women'],
        ['id' => 13,  'parent_id' => 1, 'name' => 'Kids', 'slug' => 'kids'],

        //sub-category: fashion
        ['id' => 14,  'parent_id' => 1, 'name' => 'Uniform', 'slug' => 'uniform'],
        ['id' => 15,  'parent_id' => 1, 'name' => 'Suits', 'slug' => 'suits'],
        ['id' => 16,  'parent_id' => 1, 'name' => 'Shared Service Uniforms', 'slug' => 'shared-service-uniforms'],
        ['id' => 17,  'parent_id' => 1, 'name' => 'Native Attires', 'slug' => 'native-attires'],
        ['id' => 18,  'parent_id' => 1, 'name' => 'Trousers & Shorts', 'slug' => 'trousers-and-shorts'],
        ['id' => 19,  'parent_id' => 1, 'name' => 'Shirts', 'slug' => 'shirts'],
        ['id' => 20,  'parent_id' => 1, 'name' => 'Custom Fabrics', 'slug' => 'custom-fabrics'],
        ['id' => 21,  'parent_id' => 1, 'name' => 'Gowns/Dresses', 'slug' => 'gowns-dresses'],
        ['id' => 22,  'parent_id' => 1, 'name' => 'Unconventional clothing', 'slug' => 'unconventional-clothing'],
        ['id' => 23,  'parent_id' => 1, 'name' => 'Head Gears', 'slug' => 'head-gears'],
        ['id' => 24,  'parent_id' => 1, 'name' => 'Accessories', 'slug' => 'accessories'],
        ['id' => 25,  'parent_id' => 1, 'name' => 'Shoes', 'slug' => 'shoes'],
        ['id' => 26,  'parent_id' => 1, 'name' => 'Lingerie', 'slug' => 'lingerie'],
        ['id' => 27,  'parent_id' => 1, 'name' => 'Swim Wear', 'slug' => 'swim-wear'],
        ['id' => 28,  'parent_id' => 1, 'name' => 'Co-ord Set', 'slug' => 'coord-set'],
        ['id' => 29,  'parent_id' => 1, 'name' => 'Bags', 'slug' => 'bags'],

        /* Furniture */
        ['id' => 30,  'parent_id' => 2, 'name' => 'Home Furniture', 'slug' => 'home-furniture'],
        ['id' => 31,  'parent_id' => 2, 'name' => 'Office Furniture', 'slug' => 'Office-furniture'],
        ['id' => 32,  'parent_id' => 2, 'name' => 'School Furniture', 'slug' => 'school-furniture'],
        ['id' => 33,  'parent_id' => 2, 'name' => 'Outdoor Furniture', 'slug' => 'outdoor-furniture'],
        ['id' => 34,  'parent_id' => 2, 'name' => 'Salon Furniture', 'slug' => 'salon-furniture'],
        ['id' => 35,  'parent_id' => 2, 'name' => 'Lounge Furniture', 'slug' => 'lounge-furniture'],
        ['id' => 36,  'parent_id' => 2, 'name' => 'Church Furniture', 'slug' => 'church-furniture'],
        ['id' => 37,  'parent_id' => 2, 'name' => 'Theater Furniture', 'slug' => 'theater-furniture'],
        ['id' => 38,  'parent_id' => 2, 'name' => 'Cafe Furniture', 'slug' => 'cafe-furniture'],
        ['id' => 39,  'parent_id' => 2, 'name' => 'Library Furniture', 'slug' => 'library-furniture'],
        ['id' => 40,  'parent_id' => 2, 'name' => 'Hotel Furniture', 'slug' => 'hotel-furniture'],
        ['id' => 41,  'parent_id' => 2, 'name' => 'Furniture Parts', 'slug' => 'furniture-parts'],

        /* Food */
        ['id' => 42,  'parent_id' => 3, 'name' => 'Cakes', 'slug' => 'cakes'],
        ['id' => 43,  'parent_id' => 3, 'name' => 'Small Chops', 'slug' => 'small-chops'],
        ['id' => 44,  'parent_id' => 3, 'name' => 'Special Meals', 'slug' => 'special-meals'],
        ['id' => 45,  'parent_id' => 3, 'name' => 'Catering Services', 'slug' => 'catering-services'],
        ['id' => 46,  'parent_id' => 3, 'name' => 'Fruits', 'slug' => 'fruits'],
        ['id' => 47,  'parent_id' => 3, 'name' => 'Meat', 'slug' => 'meat'],
        ['id' => 48,  'parent_id' => 3, 'name' => 'Snacks', 'slug' => 'snacks'],
        ['id' => 49,  'parent_id' => 3, 'name' => 'Vegetables', 'slug' => 'vegetables'],
        ['id' => 50,  'parent_id' => 3, 'name' => 'Drinks', 'slug' => 'drinks'],

        /* Graphics and Branding */
        ['id' => 51,  'parent_id' => 6, 'name' => 'ID Cards', 'slug' => 'id-cards'],
        ['id' => 52,  'parent_id' => 6, 'name' => 'Complimentary Cards', 'slug' => 'complimentary-cards'],
        ['id' => 53,  'parent_id' => 6, 'name' => 'Gift Cards', 'slug' => 'gift-cards'],
        ['id' => 54,  'parent_id' => 6, 'name' => 'Souvenir Printing', 'slug' => 'souvenir-printing'],
        ['id' => 55,  'parent_id' => 6, 'name' => 'Infographics & Presentation', 'slug' => 'infographics-and-presentation'],
        ['id' => 56,  'parent_id' => 6, 'name' => 'Monogram', 'slug' => 'monogram'],
        ['id' => 57,  'parent_id' => 6, 'name' => 'Stamps/Stickers', 'slug' => 'stamps-stickers'],
        ['id' => 58,  'parent_id' => 6, 'name' => 'Signages/Posters', 'slug' => 'signages-posters'],
        ['id' => 59,  'parent_id' => 6, 'name' => 'Print Media(Paper)', 'slug' => 'print-media'],
        ['id' => 60,  'parent_id' => 6, 'name' => 'Embroidery/Carving', 'slug' => 'embroidery-carving'],
        ['id' => 61,  'parent_id' => 6, 'name' => 'Animation', 'slug' => 'animation'],
        ['id' => 62,  'parent_id' => 6, 'name' => 'Web Graphics', 'slug' => 'web-graphics'],
        ['id' => 63,  'parent_id' => 6, 'name' => 'Wristband', 'slug' => 'wristband'],
      ]);
    }
}
