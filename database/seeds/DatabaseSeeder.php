<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MyCategoryTableSeeder::class);
        $this->call(CategoryChildSeeder::class);
        $this->call(ProductionScaleSeeder::class);
        $this->call(VendorTypeSeeder::class);
		    //factory(App\Product::class, 50)->create();
    }
}
