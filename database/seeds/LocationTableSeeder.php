<?php

use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $str = json_decode(file_get_contents('./state-lga.json'));
      $index = 0;
      foreach($str as $key => $value)
      {
        DB::table('states')->insert([
          ['name' => $key]
        ]);
        //echo $key;
        $index++;
        foreach($value as $item){
          DB::table('cities')->insert([
            ['state_id'=> $index , 'name'=> $item]
          ]);
          //echo $item.$index;
        }
      }
    }
}
