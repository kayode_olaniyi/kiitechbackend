<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductionScaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('production_scale')->insert([
        ['id' => 1, 'name' => 'Consumer', 'description' => 'description goes here'],
        ['id' => 2, 'name' => 'Commmercial', 'description' => 'description goes here'],
      ]);
    }
}
