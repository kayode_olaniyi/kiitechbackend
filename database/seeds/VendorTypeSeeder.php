<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class VendorTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendor_type')->insert([
          ['id' => 1, 'name' => 'Designer', 'description' => 'description goes here'],
          ['id' => 2, 'name' => 'Manufacturer', 'description' => 'description goes here'],
          ['id' => 3, 'name' => 'Fabricator', 'description' => 'description goes here'],
        ]);
    }
}
