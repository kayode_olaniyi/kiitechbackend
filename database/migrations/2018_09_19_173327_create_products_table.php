<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('price');
            $table->unsignedInteger('vendor_id');
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade')->onUpdate('cascade');
            $table->text('brief_description');
            $table->text('detail_description');
            $table->string('no_of_purchased')->nullable();//to be controlled by kiitec
            $table->boolean('is_trending')->default(false);
            $table->integer('ratings')->default(0);
            //$table->text('image_urls'); //json
            $table->timestamps();
        });

        Schema::create('product_avatars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('productmaterials_products', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('product_material_id');
            $table->primary(['product_id', 'product_material_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_avatars');
        Schema::dropIfExists('products');
        Schema::dropIfExists('productmaterials_products');

    }
}
