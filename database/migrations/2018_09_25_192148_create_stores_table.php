<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('store_name');
            $table->string('slug')->unique();
            $table->string('address');
            $table->unsignedInteger('state')->nullable();
            $table->unsignedInteger('city')->nullable();
            //$table->unsignedInteger('country')->nullable();
            $table->integer('category_id');
            //$table->integer('category_child_id');
            $table->integer('production_quantity_id');
            $table->integer('vendor_type'); //vendor is fabricator or whatever
            $table->unsignedInteger('vendor_id');
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
